from fastapi import FastAPI
from rest_handlers.genderize import GenderizeData, genderize_handler


app = FastAPI()
# app.add_middleware(HTTPSRedirectMiddleware) # this was meant to support https quick and dirty. feelfree to remove this


@app.get("/")
async def root():
    return True


@app.post("/genderize")
async def genderize(data: GenderizeData):
    return genderize_handler(data)
