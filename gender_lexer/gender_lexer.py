from pydantic import BaseModel
from typing import List


from nlp_client.nlp_client import MorphologicalFeature as MF


class ClassifiedGender(BaseModel):
    is_gendered: bool


def find_verb(mf: List[MF]):
    for word in mf:
        if word.part_of_speech == 'VB':
            return word.index
    return 0


def is_verb_related_to_noun(mf: List[MF]):
    verb_index = find_verb(mf)
    dependency_index = mf[verb_index].dependencies
    if dependency_index < 0 or dependency_index > len(mf) - 1:
        return True
    return mf[dependency_index].part_of_speech not in {"NN", "NNT", "TTL", "PRP", "QW"}


def is_singular_third_party(mf: List[MF]):
    verb_index = find_verb(mf)
    return all(f in mf[verb_index].features for f in ["num=S", "per=3"])


def check_single_third_party_no_noun(mf: List[MF]):
    return is_singular_third_party(mf) and is_verb_related_to_noun(mf)


def classify_gender(mf: List[MF]) -> ClassifiedGender:
    if check_single_third_party_no_noun(mf):
        return ClassifiedGender(is_gendered=True)
    return ClassifiedGender(is_gendered=False)
