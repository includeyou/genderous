from pydantic import BaseModel

from nlp_client.nlp_client import get_morphological_features
from gender_lexer.gender_lexer import classify_gender


class GenderizeData(BaseModel):
    text: str


def genderize_handler(data: GenderizeData):
    mf = get_morphological_features(data.text)
    return classify_gender(mf)
