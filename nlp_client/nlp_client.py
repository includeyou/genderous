import requests
from pydantic import BaseModel
from typing import List


YAP_SERVER = "http://192.168.187.64:8000/yap/heb/joint"


class MorphologicalFeature(BaseModel):
    index: int
    word: str
    part_of_speech: str
    features: list
    dependencies: int
    relation: str


def parse_yap(text: str) -> List[MorphologicalFeature]:
    features = []
    for line in text.split("\n"):
        if line:
            index, word, root, part_of_speech, _, feature, deps, relation, _, _ = line.split(
                "\t")

            features.append(MorphologicalFeature(index=int(index) - 1, word=word, part_of_speech=part_of_speech,
                                                 features=feature.split("|"), dependencies=int(deps) - 1, relation=relation))
    return features


def get_morphological_features(text: str) -> List[MorphologicalFeature]:
    response = requests.get(url=YAP_SERVER,
                            data=b'{"text": "' +
                            text.encode('utf-8') + b'  "}',
                            headers={
                                'Content-Type': 'application/json'}
                            )

    #
    dep_tree = response.json()['dep_tree']
    return parse_yap(dep_tree)
